# Mullvad Browesr and WebRTC

## Facilitator(s): ma1+rui

## Topics
- WebRTC, what's the current issue/state
- https://gitlab.torproject.org/tpo/applications/mullvad-browser/-/issues/151

## WebRTC workshop with richard@mullvad.net

We tested Mullvad's Jitsi instance with Fx 119, MB 12.5.6 and MB 13.0.1, direct and over Socks5 proxy, checking for UDP leaks:
    
1. Fx   direct : Jitsi works, UDP leaks
2. Fx   proxied: Jitsi works, UDP leaks
3. MB12 direct : Jitsi works, UDP leaks
4. MB12 proxied: Jitsi works, UDP leaks
5. MB13 direct : Jitsi works, UDP leaks
6. MB13 proxied: Jitsi BROKE, UDP blocked
    
Upon further analysis, we realized #6 behavior MAY be the correct one, caused by Mozilla having fixed their `media.peerconnection.ice.proxy_only_if_behind_proxy` pref to actually try to use the proxy as a relay for UDP traffic (https://bugzilla.mozilla.org/show_bug.cgi?id=1790270). In facts, independently from all the other webrtc prefs, if we turn off proxy_only_... Jitsi starts working and UDP leaking, like in 12.5.6.

This might mean that the Socks5 proxy doesn't actually support relaying UDP (as advertised), or that Firefox is still not doing the right thing, causing the UDP traffic to be blocked rather than relayed, or both.

To start investigating these two ipothesis we configured a Torrent client which advertises Socks5 compatibility to constrain all its traffic to the tunnel, but we found this application "lied", i.e. a lot of UDP traffic leaked outside the tunnel.

Therefore we decided to proceed in parallel along 3 lines:
    
1. Open a new Mozilla bug, linked to https://bugzilla.mozilla.org/show_bug.cgi?id=1790270, about making videoconferencing actually work with media.peerconnection.ice.proxy_only_if_behind_proxy and Mullvad VPN active (of course without UDP leaks), under the assumption Mozilla VPN would benefit as well from the fix
2. Check wether Mullvad VPN is actually able to relay UDP traffic (as it was believed up to today), and if not, understand if/how this can be fixed
3. As a fallback (or short-term) plan, explore UX to warn user about their IP being leaked via UDP to the WebRTC infrastructure before the leak start and allow them to opt-in/opt-out the session
    
rui & ma1 will take care of #1, richard@mullvad.net is doing #2, ma1 is going to figure out #3 with the UX team.
    
    