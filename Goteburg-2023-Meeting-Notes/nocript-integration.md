# NoScript Integration in base-browser

## Facilitator(s): ma1

## Issues

- https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40925

## Topics

- Chat about OTF grant/funding proposal for NoScript + Browser integration
- Related issues:
    - https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40925
    - https://gitlab.torproject.org/tpo/operations/proposals/-/issues/53

# Goals

- determine technical+ux benefit and feasibility of such a project
- roadmap/outline of work required
- scope it down/outline roadmap for OTF grant

## History of NoScript integration in base-browser

Originally, it was controversial to include, there were a couple concerns with it raised by MikePerry - we worked to make it more suitable. Originally, the HTTPs-everywhere features were provided by NoScript. Modifications that ma1 made were borrowed by EFF and lifted into https-everywhere. 

At some point UX people decided that NoScript was a good idea to have course level of restrictions, which was a compromise for bucket changes. 

At this point NoScript drives the security settings.

## TODO

- UX research around users of `Safest` security level, how they use it, what they are expecting, are they using it `correctly`
- Review the security level prefs based on our threat model
- Enumerate the NoScript UX we would need to implement

    - noscript.net/usage

- Connect Mullvad Matilda w/ Donuts

OTF has funded both TB and NoScript in the past, so they might be interested in doing something together. Maybe targeting something for 14.5 release cycle. 

Determine technical+ux benefit and feasibility of such a project
----------------------------------------------------------------
First thing to do would be to decouple the experimental TB version from the other version, so we are independent of the Mozilla infrastructure. In the past there were issues where FF couldn't fix a bug that was important to us, so we had to do a work-around in NoScript, before the security issue was released to the public. 

Secondly, spend time with donuts/harry about how to change the UI to understand if and what should be surfaced, and how, and harmonize that with the Tor branding, because otherwise you get the NoScript logo. Could be 'brand-less' or branded with the existing security level.

## Potential Benefits/Problems

- NoScript curently has no access to modal dialogs, creates new Firefox windows for popups (ew)
- We could somehow alert users if they try to install the Firefox AMO version of NoScript on-top-of/in-place-of our bundles version/features

## per-site slider discussion

Thorin: There is a long-standing ticket that we turn the security slider to per-site/per-tab. Is there any limitation in NoScript for doing this per-site? 

r: The security slider divides the responsibility between browser native stuff and NoScript stuff. For the NoScript part, we could do that, but the other things are global firefox settings. 

ma1: We should review the preferences one-by-one to see if we can do this

r: would be desirable to have it per-site, but don't think its achievable for us

ma1: it would require some hours of analysis, as a review of the threat modeling we should do this review anyway.

Summary:
    Update channel
    de-branding of UI in general
    native dialogs on desktop
    possibly evaluate per-site slider
    
## discussion of making NoScript a first-party/system extension

Unknown if this can be done
Benefits: doesn't show in about:addons, if you want to hide the noscript interface, this might be something to investigate the UX benefits of it. 
Does this add more permissions? We don't want more permissions for NoScript
if its integrated in the code, it can be integrated in modal dialogs and the browser itself.

## Android
We should be considering this platform in everything that we are doing on the desktop
In terms of NoScript: if we talk about per-site security slider, we should figure out where to put it. 
NoScript is more visible on TBA than desktop.
Its currently doing everything on a security backend, its being used.
On-boarding is not asking people about this, this should be fixed in 13.5
NoScript is currently not a first-class UI citizen, like it is on Desktop. If it were to be the same as desktop

## Merging the slider and NoScript to hide NoScript entirely
Hide NoScript and just make the slider handle that. The interface is confusing for people, and can create problems. Would be better to completely hide it permanently, and ideally make it so we have per-site slider settings, this is essentially the NoScript/slider button.

ma1: i like this idea, but it doesn't work - you should clearly distinguish a global security level from a local security level. Different "Safest global" vs. "Standard on this site" - these are the complete opposite model. It should communicate clearly what the global vs. local settings, because you forget what your default security mode is, if I open another window, what happens.

richard: we thought a lot about why we made it so you can't change it on the toolbar, because this can be confusing about what your default setting is.

rui: it might be more confusing for NoScript users, but not sure how many people would be confused by this. Make the default/easy option should be the default and figure out the more complicated things later.

ma1: we should discuss more this new model, it probably was discussed and abandoned in the past, we should review issues etc. Maybe a radical proposal is default deny for everyone, and then this button is locally changing things (button to unbreak the site).

thorin: in FF you have the ability to block all cookies, and you allow some - or the reverse. the security slider toolbar button is global, but if its per-site you do it in the URL bar. But we still need users to be able to know or know about their default global state, but (if we can do per site), to toggle and reload via the urlbar icon for each site/exception

there is a distinction in FF where global settings are in one place, and per-site are in another place. We had to transition from tor-button, but now that things are integrated, we have more freedom to put URL bar per-settings, and global settings in the toolbar.

h: is that really the paradigm? most of the toolbar items work on the tab specific level
<discussion of if this is true or not, and history>

r: what do we actually want to do depends on threat model and UX research

ma1: i would work on update channel

## Actionable points:
- more UX discussion
  - poke duncan for UX research, especially around safest level
- rebranding
- per-site security
- don't want to confuse current users with current usage set and then provide them with features that are more appropriate to our revised threat model
- Matilda is asking how she can help with UX issues, so connect her with duncan.
- update channel