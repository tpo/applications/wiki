Tor Browser provides a built-in automatic updating process. This is currently the only supported way of maintaining an updated browser.

The update URL is: https://aus1.torproject.org/torbrowser/update_3/

The current versions can be seen using this URL (stable and alpha, respectively):
 - https://aus1.torproject.org/torbrowser/update_3/release/downloads.json
 - https://aus1.torproject.org/torbrowser/update_3/alpha/downloads.json

On Unix-like systems, piping this through `jq` is suggested:
```
curl https://aus1.torproject.org/torbrowser/update_3/release/downloads.json | jq -r . | less
```

Current updates (on Linux 64-bit, en-US locale) use a URL like:
 - https://aus1.torproject.org/torbrowser/update_3/alpha/Linux_x86_64-gcc3/9.5a2/en-US

When updating from a version of Tor Browser older than 8.0, due to the watershed the URL is something like:
 - https://aus1.torproject.org/torbrowser/update_3/alpha/Linux_x86_64-gcc3/9.5a2/en-US

# Update Watersheds
Over the years since Tor Browser began using automatic updates, the requirements of updating changed a few times. These changes were implemented by migrating to a new update location. These were watershed updates.

| Ticket | Path | Release |
|--------|------|---------|
| #19316 | update_3 | 6.5.2 |
| #26050 | update_3 | 8.0a10 |
| tor-browser-build#40678, #41414 | update_3 | 11.5.8 |

# History

MAR files were signed beginning in 2014, with ticket #13379. Incremental mar files were implemented in #13324.

# Manually Applying MAR Update
These instructions are based on https://wiki.mozilla.org/Software_Update:Manually_Installing_a_MAR_file

## Linux:

1. We assume the Tor Browser installation directory is `/path/to/torbrowser`, this is the directory normally named `tor-browser_${LOCALE}` (like `tor-browser_es-ES/` and it contains the `Browser/` directory)
1. Create a directory somewhere your user can read/write/execute, such as in your home directory (let's call it `app-update`).
1. Download incremental MAR file, such as `tor-browser-linux64-9.5a6-9.5a7_es-ES.incremental.mar` (most likely from https://dist.torproject.org/torbrowser/)
1. Move .mar file into the `app-update` directory
1. Rename .mar file as `update.mar`
1. Copy `updater` and `updater.ini` into the same directory as above (`cp /path/to/torbrowser/Browser/updater* /path/to/app-update/`)
1. Change directory into the `app-update` directory (`cd /path/to/app-update/`)
1. Run updater (`LD_LIBRARY_PATH=/path/to/torbrowser/Browser ./updater /path/to/app-update /path/to/torbrowser/Browser /path/to/torbrowser/Browser`)
  1. This requires setting LD_LIBRARY_PATH with the directory containing Tor Browser's Firefox libraries
  1. Replace `/path/to/torbrowser` with the correct path on your computer
  1. ALL PATHS MUST BE ABSOLUTE, FULL PATHS beginning with "/"
1. After this command is executed, an `update.status` file should be created in `app-update/`. If the update was applied successfully, then the file should contain "succeeded".
1. After successfully applying the update, simply delete the `app-update/` directory

## MacOS:
1. Create a directory somewhere your use can read/write/execute, such as in your home directory (let's call it `app-update`).
1. Open `Applications` and open the context menu of `Tor Browser.app`, select "Show Package Contents`
1. After showing the package content, open `Contents/MacOS/` and copy `Tor Browser Software Update` (`updater.app`) into `app-update` (created above)
1. Download incremental MAR file, such as `tor-browser-osx64-9.5a6-9.5a7_es-ES.incremental.mar` (most likely from https://dist.torproject.org/torbrowser/)
1. Move the downloaded .mar file into the `app-update` directory
1. Rename .mar file as `update.mar`
1. Open a `Terminal` and change directory into the Tor Browser installation (`cd /Applications/Tor\ Browser.app/`)
1. Run the updater program (we assume the `app-update` directory is in your Home, and set `DYLD_LIBRARY_PATH` as directory where the libraries are located
  1. `$ DYLD_LIBRARY_PATH=Contents/MacOS/ ~/app-update/updater.app/Contents/MacOS/org.mozilla.updater ~/app-update /Applications/Tor\ Browser.app/ /Applications/Tor\ Browser.app/` (the installation directory is provided twice in this command)
1. After this command is executed, an `update.status` file should be created in `~/app-update/`. If the update was applied successfully, then the file should contain "succeeded".
1. After successfully applying the update, simply delete the `app-update/` directory

## Windows:

**NOTE**: These steps assume you installed Tor Browser in the default installation location: `%USERPROFILE%\Desktop\Tor Browser`. If you have installed Tor Browser somewhere else, you will need to update the commands referencing the Tor Browser installation directory to point to your custom installation path. Also note that `%USERPROFILE%` expands to the current user's home directory. For most users this is `C:\Users\%USERNAME%`.

1. Ensure that Tor Browser is not running.
1. Open an un-elevated command prompt by `Win+R`, typing `cmd.exe` in the resulting prompt, and hitting the `OK` button.
1. From the command prompt, create a temporary directory by:
    - `mkdir %USERPROFILE%\app-update`
1. Download incremental MAR file, such as `tor-browser-win64-9.5a6-9.5a7_en-US.incremental.mar` (most likely from https://dist.torproject.org/torbrowser/) and save it in your `app-update` directory.
1. Rename your downloaded MAR file to `update.mar`
1. Copy the `updater.exe` shipped with Tor Browser to your `app-update` directory:
   - `copy "%USERPROFILE%\Desktop\Tor Browser\Browser\updater.exe" %USERPROFILE%\app-update`
1. Change your working directory to your Tor Browser installation path:
   - `cd "%USERPROFILE%\Desktop\Tor Browser\Browser"`
1. Run the updater:
   - `%USERPROFILE%\app-update\updater.exe %USERPROFILE%\app-update "%USERPROFILE%\Desktop\Tor Browser\Browser" "%USERPROFILE%\Desktop\Tor Browser\Browser"`
1. After this command is executed, an update.status file should be created in `%USERPROFILE%\app-update`. If the update was applied successfully, then the file should contain "succeeded". You can verify this by:
   - `type %USERPROFILE%\app-update\update.status`
     In the event of error, you can examine `%USERPROFILE%\update.log` to troubleshoot the problem.
1. Cleanup the app-update directory
   - `rmdir /S /Q %USERPROFILE%\app-update`

## MAR Troubleshooting

The provided updater program is not very user friendly and will not provide immediate user feedback in the event of failure. If the `update.status` file says anything besides "succeeded" you can examine the generated `updater.log` in your created `app-update` directory for clues. The most likely cause of error is using a MAR file that is not compatible with your Tor Browser installation. Unfortunately there are multiple ways in which you can use the wrong MAR file:

### CPU an OS Architecture

First, you must be sure that the OS and and CPU architecture match between the MAR file and your Tor Browser installation:

| **MAR** | **Architecture** |
|---------|------------------|
| linux32 | 32-bit x86 Linux |
| linux64 | 64-bit amd64 Linux |
| osx64 | 64-bit macOS |
| win32 | 32-bit Windows |
| win64 | 64-bit Windows |

You can determine 'bitness' of your Tor Browser installation by launching Tor Browser and navigating through `Hamburger Menu -> Help -> About Tor Browser`. There should be a line in the resulting pop-up similar to `9.5a7 (based on Mozilla Firefox 68.5.0esr) (64-bit)`. Please note that it is typically possible to run 32-bit software on a 64-bit OS, so please be sure to check you have this right if your update fails.

Just because you are running 64-bit Windows or Linux does not mean you need the 64-bit MAR file!

The updater.log file will likely complain about mismatched file names if you use a MAR file for the wrong OS. It will likely complain about mismatched file sizes if you use a MAR file with the wrong CPU architecture.

### Correct Incremental Version

Second, if you are updating using an incremental MAR file (these MAR files end in `.incremental.MAR`) be sure that the 'from' version matches your Tor Browser installation's version. The incremental mar file's have a filename in the form: `tor-browser-$ARCH-$FROM_VERSION-$TO_VERSION_$LOCALE.incrementalmar`. For example, an incremental update from the American English 32-bit Windows Tor Browser version 9.5a6 to version 9.5a7 has the filename: `tor-browser-win32-9.5a6-9.5a7_en-US.incremental.mar`

You can determine the version number by launching Tor Browser and looking at the top-right hand corner of `about:tor` (for left-to-right languages). The version can also be found in the `About Tor` dialog as described in the previous section.

The `updater.log` file will likely complain about mismatched file sizes if you use the wrong incremental version.

# Testing Updater

Testing the updater (and update process) is non-trivial. There are three separate modifications that must be made within the Tor Browser build environment.

1. Update URL
1. Signature public key
1. Nightly build configuration file

In addition, you must [create a new public key pair](https://gitlab.torproject.org/tpo/applications/tor-browser-spec/-/blob/main/processes/KeyGeneration#L41) for signing mar files. Copy the resulting public certificate  `marsigner.der` for later use, too.

## Updater URL and Signature Public Certificate

Testing the `nightly` channel is easiest.
`alpha` and `release` are also feasible, but slightly less automated because our tools take for granted that you are updating from signed builds.

You can change the URL in the [build configuration](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/blob/main/projects/firefox/config#L38): you just need to set `override_updater_url`.
This URL will contain the update responses, that are some XML files that the Firefox updater polls with a certain frequency to check if any update is available.

Setting a custom updater URL will also override the update certificate: copy yours to `project/firefox/marsigner.der`.

If you are testing `release` or `alpha`, you will have to modify the `build` script to replace the release key instead of the nightly.

Since you are creating new builds for testing purposes, it is a good idea to add some code to help you to debug the updater binary.
See the troubleshooting section.

## Sign MAR file

```sh
mar_tools="path/to/mar-tools"
nssdb="/path/to/nssdb"
marfile="/path/to/tor-browser-linux64-testbuild1_en-US.mar"
mkdir signed

$mar_tools/signmar -d $nssdb -n marsigner -s $marfile signed/$marfile
```

## Verify Signed MAR file

### Using the same database containing the private key material

```sh
mar_tools="path/to/mar-tools"
nssdb="/path/to/nssdb"
marfile="/path/to/tor-browser-linux64-testbuild1_en-US.mar"
mkdir signed

$mar_tools/signmar -d $nssdb -n marsigner -v signed/$marfile
```

### Using a separate database with only the public certificate

#### Create a new database with the public certificate

```sh
nssdb="/path/to/nssdb"
mkdir $nssdb
mar_tools="path/to/mar-tools"
pubkey="/path/to/marsigner.der"
$mar_tools/certutil -A -n marsigner -d $nssdb -t ,, -i $pubkey
```

#### Verify the signature

```sh
mar_tools="path/to/mar-tools"
nssdb="/path/to/nssdb_public"
marfile="/path/to/tor-browser-linux64-testbuild1_en-US.mar"
mkdir signed

$mar_tools/signmar -d $nssdb -n marsigner -v signed/$marfile
```

## Prepare the update responses

As written above, we need to create the XML files to tell Firefox where it can find our mar files.

In production, we use a few tools in `tor-browser-build` to create the update responses, let's use them also for our test!

They are located in `tools/update-responses`.

### Generate the incrementals

Generating the incrementals has the "side" effect of creating a configuration to create the update responses.
For nightly you can use use this command:

```shell
make torbrowser-incrementals-nightly
```

The resulting incremental mars are not signed (maybe we could add a `config.yml` entry to specify the nssdb path and the nickname of the key to use), so you will need to sign them with the instructions you can find above.

Otherwise, if you really don't want to generate the incrementals, you can simply use this command to make the updated configuration:

```shell
rbm/rbm build release --step update_responses_config --target nightly --target torbrowser
```

For `release` and `alpha`, you can use these `make` targets:

```shell
make torbrowser-incrementals-alpha-unsigned
make torbrowser-incrementals-release-unsigned
```

Otherwise, you will have to use the above command to generate the configuration first, adjust it to use the unsigned build directory:

```yaml
versions:
    xx.y.z:
        releases_dir: .../tor-browser-build/torbrowser/release/unsigned
```

Then you can optionally generate incrementals manually with:

```shell
tools/update-responses/gen_incrementals $channel
```

#### Update the URLs in `config.yml`

`tools/update-responses/config.yml` is a non-versioned file that is generated automatically at the previous stage. \
However, by default it uses the data for the actual releases, so you will very likely need to update your file.
Updating the `download` section in this way should be enough:

```yaml
download:
    bundles_url: https://tb-build-03.torproject.org/~pierov/torbrowser/nightly
    mars_url: https://tb-build-03.torproject.org/~pierov/torbrowser/nightly
```

or

```yaml
download:
    bundles_url: https://tb-build-03.torproject.org/~pierov/torbrowser/alpha/unsigned
    mars_url: https://tb-build-03.torproject.org/~pierov/torbrowser/alpha/unsigned
```

`~/public_html/torbrowser` in this case is a link to the `torbrowser` directory in the clone of `tor-browser-build` when I ran the build.

If you have used a build server, but are creating the update responses locally, you can use the `releases_dir` property to tell the scripts where they can find your builds.
It must contain at least the `.mar` archives and the `mar-tools`.

### Generate the update responses

Before creating the update responses, **be sure of having signed the `.mar`s**: Tor Browser update responses will contain hash ans size, and signing will change them.

If you have done that, creating the update responses is very easy, just run this command:

```sh
./update_responses nightly
```

If you are on another channel, you should replace nightly with that.

When it is done, copy the content of the `htdocs` directory to the URL you provided to the build (e.g., `~/public_html/update_3`):

```shell
cp -r htdocs/nightly ~/public_html/update_3/
```

At this point, Firefox should find the update automatically, download it, and apply it.

### Troubleshooting

#### Logs

Updater logs can be enabled in the browser by setting `app.update.log` to `true`, and are visible in the JavaScript console.

This helps to find deploy and/or download errors (e.g., wrong URL in the `config.yml`, unsigned mars etc...).

Errors that occurred during the update process are usually logged in `UpdateInfo/updates/last-update.log`.

#### Attaching a debugger

The `updater` process is started by Firefox, therefore it is very difficult to start it in a debugging environment.

There are a couple of workarounds that involve modify adding some code at the beginning of `NS_main` in `toolkit/mozapps/update/updater/updater.cpp`.

If you are on Windows, you can add a `__debugbreak();` line, and you will be able to attach the debugger.
However, if you can't debug it, there is no way to resume the updater.

A similar approach is to add some sleep, whose value depends on an environment variable.
For example:

```c++
#include <thread>

int NS_main(int argc, NS_tchar** argv) {
  const char *shouldDelay = getenv("TBB_UPDATER_DELAY");
  if (shouldDelay) {
    char *endptr;
    long d = strtol(shouldDelay, &endptr, 10);
    if (endptr == shouldDelay) {
      d = 60;
    }
    std::this_thread::sleep_for(std::chrono::seconds(d));
  }
```

For other platforms, you will need to change `Sleep` with something else (maybe `std::this_thread::sleep_for` will work).

At that point, you can define a number of seconds to sleep and let you attach the debugger by setting `TBB_UPDATER_DELAY` before running the Firefox process.
Luckily, Firefox will pass the environment variables to the updater process.
