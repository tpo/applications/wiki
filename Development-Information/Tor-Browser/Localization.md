Tor Browser borrows its localization approach from Mozilla. Therefore, in tor-browser we now use [the Fluent format](https://www.projectfluent.org/) for strings that need translation. There are some legacy strings that use the [DTD](https://en.wikipedia.org/wiki/Document_type_definition#Entity_declarations) or [properties](https://en.wikipedia.org/wiki/.properties) formats. For Android, we use [Android string resources](https://developer.android.com/guide/topics/resources/string-resource).

The strings for the default "en-US" locale (English US) are all kept in the tor-browser repository. These "en-US" files are copied into the [translation repository](https://gitlab.torproject.org/tpo/translation), where the translation for every other locale is managed. This repository manages the strings of various Tor Project works, but Tor Browser mostly uses the [base-browser](https://gitlab.torproject.org/tpo/translation/-/tree/base-browser) and [tor-browser](https://gitlab.torproject.org/tpo/translation/-/tree/tor-browser) branches for Tor Browser, as well as [fenix-torbrowserstringsxml](https://gitlab.torproject.org/tpo/translation/-/tree/fenix-torbrowserstringsxml) for Tor Browser for Android.

**NOTE:** If you simply wish to contribute a translation, visit [the Tor Project localization page](https://community.torproject.org/localization/).

# Fluent

Almost all new strings for tor-browser should use [the Fluent format](https://www.projectfluent.org/).

For example:

```fluent
user-welcome-intro = Welcome { $user }!
deactivate-button = Deactivate
    .accesskey = D
    .title = Permanently close your account
```

Here `use-welcome-intro` is the Fluent ID for a message with the value "Welcome { $user }!". The Fluent variable `$user` will be substituted with the value of `user` passed in.

And `deactivate-button` is the Fluent ID for a message with the value "Deactivate". It also has two attributes: `accesskey` and `title`. If this message is assigned to a DOM element, the message value will be used as the `innerHTML` and the attribute values will be used for the corresponding HTML attribute.

If you are not familiar with Fluent, then you can [read the syntax guide](https://projectfluent.org/fluent/guide/). Mozilla also provides [a tutorial](https://firefox-source-docs.mozilla.org/l10n/fluent/tutorial.html) and some [further guides](https://firefox-source-docs.mozilla.org/l10n/fluent/review.html).

## Files

We currently have two files for Fluent strings: `base-browser.ftl` and  `tor-browser.ftl`, depending on whether the string is used in a [Base Browser](https://gitlab.torproject.org/tpo/applications/team/-/wikis/Development-Information/Tor-Browser/Tor-Browser-Repository-Overview#base-browser) feature or not.

We also have two other files: `tor-browser-no-translate.ftl` and `base-browser-no-translate.ftl`. These should be used for strings that should *not* be translated yet. For example, if an area of the UI is still under development and likely to change in the near future, then you can include your strings in one of these files instead so that translators do not waste their time on them. Once the strings are stabilised, they can be moved into `base-browser.ftl` or `tor-browser.ftl`.

## String IDs

The Fluent IDs need to be unique across all strings in `base-browser.ftl`, `tor-browser.ftl` and all the Mozilla strings. They should also be somewhat descriptive.

## Avoid "Tor Browser"

Generally, if you wish to refer to "Tor Browser" as an application name, you should use the Fluent term reference `{ -brand-short-name }`. This will allow forks to redefine the value of the `-brand-short-name` term to re-brand the browser. In particular, Mullvad Browser can set `-brand-short-name` to be "Mullvad Browser".

## Comments

Fluent allows you to add [comments for messages](https://projectfluent.org/fluent/guide/comments.html). Individual comments for messages can be added with lines starting with `#`. You can also use group comments, that start with `##` to apply a comment to all subsequent messages. You should end a group comment's scope with a new group comment, or an empty group comment: `##`.

### Comments for translators

Message and group comments will be shown to translators, so should only be relevant for them. These can be used to give some additional context.

For example, if an English word has multiple meanings, or can be interpreted as either a noun or verb, you may wish to clarify which you mean. Moreover, whenever you include a Fluent variable (e.g. `$var`) you should explain what the variable represents, and its type (String or Number).

**NOTE:** Comment lines will be concatenated for translators, disregarding line breaks. So you should end each comment line with a full-stop `.`.

For example:

```fluent
## Tor Bridges Settings.

tor-bridges-none-added = No bridges added

# "Gmail" is the Google brand name. "Riseup" refers to the Riseup organisation at riseup.net.
tor-bridges-provider-email-name = Gmail or Riseup
# Here "Email" is a verb, short for "Send an email to". This is an instruction to send an email to the given address to receive a new bridge.
# $address (String) - The email address that should receive the email.
# E.g. in English, "Email bridges@torproject.org".
tor-bridges-provider-email-instruction = Email { $address }

##

# Shown in Home settings, corresponds to the default about:tor home page.
home-mode-choice-tor =
    .label = Tor Browser Home
```

Translators will be able to see the comment for `tor-bridges-none-added` as:

> Tor Bridges Settings.

Whilst `tor-bridges-provider-email-instruction` will be:

> Tor Bridges Settings. Here "Email" is a verb, short for "Send an email to". This is an instruction to send an email to the given address to receive a new bridge. $address (String) - The email address that should receive the email. E.g. in English, "Email bridges@torproject.org".

And `home-mode-choice-tor` will be:

> Shown in Home settings, corresponds to the default about:tor home page.

### Comments for developers

If you add a blank line after a comment that starts with just `#`, it will not be seen by translators. For example:

```fluent
# Comment only for developers.

# Comment for translators.
my-fluent-string = Some text
```

### Strings and browser versions

For Tor Browser, we need to allow translators to translate the strings used in the next nightly build, as well as the next stable build.

However, some strings will only be available in one version, and not the other. As such, we combine the translation files found in the nightly build with those in the stable build into one file. This combined file will be passed on the translators (via Weblate) to localize. The localized file is used in the final build, and will contain the strings needed for both versions.

Generally, developers do not need to worry about this, and can add or remove strings from the current development branch. When these changes land in the development branch they will trigger a CI pipeline which updates the combined file automatically.

### Adding new strings

If you are adding an entirely new string, then simply add it to the corresponding file, near any related strings.

Just make sure that the Fluent ID is unique and unlikely to clash with other strings coming from Mozilla.

### Deleting strings

If a string is no longer used, simply remove it from the translation file. It will remain available to translators until it is also no longer used in the stable release, albeit with an automated comment mentioning its limited lifetime.

### Modifying strings

Sometimes, you may wish to adjust the wording of an "en-US" Tor Browser string. Almost all of the time, you should use a completely new Fluent ID for the new string, and delete the old string. This ensures that translators can translate the new value, and the old value will stop being used in other languages.

If the wording also needs to change in the next stable branch, then the change should also be backported to the stable branch.

### Adding or removing attributes

If you just wish to add or remove Fluent attributes, then this counts as modifying the string. So a new Fluent ID will be required.

### Migrating Strings

Sometimes a string only needs to be migrated, rather than modified. For example, if we switch an element from being a `xul:button` to a `html:button`, we may wish to migrate the string from:

```fluent
old-xul-string =
  .label = My Button Text
```

to:

```fluent
new-html-string = My Button Text
```

Similarly, we may want to copy over strings from one (legacy) translation file to another. Situations like these may warrant using a migration script to automate this migration for other locales as well.

We have a migration script in `./tools/torbrowser/l10n/migrate.py` which can run one of the migration recipes found in `./tools/torbrowser/l10n/migrations/`. These migrations are run once per locale in the translations repository. A lot of this is borrowed from the [Mozilla migration tool](https://firefox-source-docs.mozilla.org/l10n/migrations/index.html).