# Searching through Files

If you want to understand some behaviour in Tor Browser that is inherited from Firefox, then the [Mozilla searchfox website](https://searchfox.org) is a great resource for searching through Mozilla code. Make sure to choose the `mozilla-esr` repository for the current ESR release used in Tor Browser. The site offers lots of search and blame tools, which can often help with tracking down some behaviour. Just be aware that it does not contain any of the patches we add for Tor Browser, so you may need to consider any alterations we have made in tor-browser.

If you want to search through our Tor Browser patches, we have two tools. First, you can use:

```sh
./tools/torbrowser/tb-dev files-containing <some-regex>
```

which will search through the code for any occurrences of the regex. It will only search through files that have been modified for Tor Browser, so it will exclude files from Mozilla that have not been touched by us. This is often useful for determining where a module, class or function gets used.

Alternatively, you can just directly look through all of our patches with:

```sh
./tools/torbrowser/tb-dev log -- -p
```

And then you can search the content of the log for whatever you like.

# Console Debugging

Tor Browser comes with a powerful debugging tool that is build into Firefox: the [Browser Toolbox](https://firefox-source-docs.mozilla.org/devtools-user/browser_toolbox/index.html). This is similar to the [Web Developer Tools](https://firefox-source-docs.mozilla.org/devtools-user/) used for inspecting web pages, but has a wider scope which allows you to inspect the browser "chrome": the UI outside of web pages. The tool needs to be enabled before you can use it, a ["dev" build](./dev-Build) will have it automatically enabled, whilst a release, alpha or nightly build [requires some steps to enable it](https://firefox-source-docs.mozilla.org/devtools-user/browser_toolbox/index.html#enabling-the-browser-toolbox).

The console part of the toolbox can be also opened by adding the `-jsconsole` flag to the browser command line.
It is especially helpful when the browser does not start because of startup problems in some JS modules (including syntax errors).

## Android

Android also has the possibility to do some debugging: after you enable USB debug on your device/emulator, you can connect to it by opening `about:debugging` on Firefox.

However, it's more limited than the desktop console.
The Android console does not include global objects such as `ChromeUtils` and `Services`, therefore it does not allow to import our modules in the console and do some "hacking on the fly".

The objects printed in the logs can be accessed in a limited way.

# Native debugging

## Getting pids

Firefox uses many processes.
Crashes that make a window close/not respond are usually on the parent process.
Crashes that makes the "Gah. Your tab just crashed." message appear are usually child processes.

Debugging the parent process is much easier: you can just launch a debuger on it.
Crashes that happen on a child process are usually more difficult to debug.

Notice: there are several kind of child processes, in particular privileged (some about pages) and unprivileged content process (normal websites).
Then there are other kind of child processes.

A trick to attach a debugger to a content process is to open a site that is known for not causing a crash, check its pid in `about:processes` (it is the number between parentheses), pass it to the debugger and then open the offending website in the same tab.

Please notice that Firefox will change process if you switch from a privileged page (`about:tor`, etc...) to an unprivileged one.

Linux debug builds are much easier to work with, and you will get the process to attach to in the standard output.
Firefox will start sleeping indefinitely, so that you can attach a debugger and have the full stack trace.

## Getting JS stack traces

Firefox allow privileged JS to easily call native code through XPCOM.

Finding what was happening on the JS side from a native stack trace is pretty difficult, if not impossible.
The solution is to a function called `DumpJSStack()` from your debugging prompt.
It will dump also the JS stack.

## Debugging tor-browser-build builds with debug symbols

For some platforms, we ship debug symbols that can be used to debug the builds created with tor-browser-build.

In all cases, you can get the commit hash used for the build in `about:buildconfig`.

### Linux, x86_64

We ship debug symbols on Linux (at the moment of writing, [only for x86_64](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42146)).

You can download `tor-browser-debug-symbols-linux-x86_64-vvv.tar.xz` from the same directory from which you got the browser tarball.

You can rename the `Debug/Browser` to `..../tor-browser/Browser/.debug`, define a few environment variables (`HOME=.../tor-browser/Browser`, `FONTCONFIG_PATH` and `FONTCONFIG_FILE`), then you will be able to start the browser in a debugger (e.g., `gdb ./firefox.real`, but you can also use LLDB if you prefer).

Both GDB and LLDB can attach to an existing process, if you provide a pid.

The debugging experience will be somehow limited, as many variables will be optimized out, but it should be enough to reproduce crashes.

Also, before running, you might want to remap the source path on GDB if you want to set breakpoints.

The debug tarball also includes generated headers, if needed.

### Windows

For Windows, we ship PDB files which can be used with Microsoft Visual Studio.
You can download the zip file and extract it wherever you prefer.

Then you can launch the browser and launch Visual Studio (the community version is fine; in any case choose to proceed without code).
You will have an empty Visual Studio window, from which you will be able to attach to a running process (either parent or child), choose the one you want to debug.

At the first crash, Visual Studio will prompt you for the PDB files, you can just point it to the directory where you extracted them.

It will also prompt you for the path to the source code. It should happen only twice: one the first time you hit a file from the source tree, and one the first time you hit a generated header.

## Debug builds

If you want to run the program through an external debugger, you can sometimes follow the [debugging guides for Firefox](https://firefox-source-docs.mozilla.org/contributing/index.html). Often, you will want to change a [flag in your `mozconfig` file](https://firefox-source-docs.mozilla.org/setup/configuring_build_options.html#optimization) for debugging.

Some useful options are:

```sh
ac_add_options --enable-debug
ac_add_options --enable-debug-js-modules
ac_add_options --disable-optimize
ac_add_options --disable-rust-simd
```

Please notice that at the moment of writing (end of 2023) [Bug 1860020](https://bugzilla.mozilla.org/show_bug.cgi?id=1860020) makes our builds with assertions enabled useless.
The quick fix is to comment the content of the `CheckTelemetryPref` function in `modules/libpref/Preferences.cpp`.

Otherwise, the binaries built with `mozconfig-linux-x86_64-dev` are unstripped and will allow you to do the same debug you could do in our official builds.

NOTE: If you need to debug some C++ code, you may be able to use the Browser Toolbox if the part you are interested in is [exposed through some IDL](https://firefox-source-docs.mozilla.org/xpcom/xpidl.html) or [Web IDL](https://firefox-source-docs.mozilla.org/dom/webIdlBindings/index.html) and [accessible in the browser console through javascript](https://firefox-source-docs.mozilla.org/xpcom/writing-xpcom-interface.html#high-level-overview).

# Browser's standard output

Usually, the browser's standard output is not very helpful, but if needed it can be enabled with adding the `-v` flag to the `start-tor-browser` script.

It needs to be the first one, because the rest of the flags will be passed directly to the `firefox` binary.