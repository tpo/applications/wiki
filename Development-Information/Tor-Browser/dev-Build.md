If you just want to contribute to the tor-browser repository, it is normally sufficient to test your changes in a local "dev" build for your platform. A dev build is a way to quickly build Tor Browser using your local changes to tor-browser, without having to build all the other components that go into a full Tor Browser distribution.

It works by compiling your local changes to tor-browser, which is normally used to build the "firefox" component of Tor Browser, and grafting them into an existing full Tor Browser distribution (usually some alpha build of Tor Browser). As such, all your changes to tor-browser should be rebuilt, but the other components of Tor Browser, like the `tor` program, will be borrowed from some existing release.

If you need to test changes with a complete release, for a different platform, or with changes outside of tor-browser, you may need to perform a [full build using tor-browser-build](/Development-Information/Tor-Browser/Building).

The content of this page is continuously exercised on Linux, where almost all desktop developers work, but it should work also on macOS (the `Makefile` was tested and adapted to work also on macOS, at some point).
Please notice that macOS builds created in this way use a very different toolchain from what we use for official builds.

A similar approach works also for Windows, but it involves cross-compilation on a Linux box.
[This page](Development-Information/Tor-Browser/Windows-dev-builds) contains information on how to do it.
Theoretically, it should be possible to build Tor Browser with clang-cl, since our C++ changes are almost trivial.
However, we have never tried to do so.

For Android, it is more complicated, because you will have to build a patched GeckoView first, and then use it to build the patched Firefox Android.
[This page](Development-Information/Firefox-Android/Building) contains all the Android-specific information.

# Firefox mach

A lot of the following commands make use of [the `./mach` tool](https://firefox-source-docs.mozilla.org/mach/usage.html) provided by Mozilla, and is included in the Firefox repository. It is used in Firefox for many development tasks like building, linting and contributing. For tor-browser, you won't necessarily use this command as often as a Firefox developer, but we have a lot of tools which use `./mach` underneath.

# Prerequisites

If you do not want to resolve build dependencies yourself, you can borrow the `./mach bootstrap` tool from the Mozilla Firefox codebase to automatically do this for you based on your system. Just note that it was written with Firefox development in mind.

In your `tor-browser` directory (where `mach` lives), run

```sh
./mach bootstrap
```

When it asks which version you would like to build, you can choose "Firefox for Desktop". Tor Browser doesn't support [artifact builds](https://firefox-source-docs.mozilla.org/contributing/build/artifact_builds.html) like Firefox does.

You can decline any further prompts about whether to configure Git, if you will be submitting to Mozilla, and enabling build system telemetry.

# Build Configure

Firefox uses `mozconfig` files for [configuring various builds](https://firefox-source-docs.mozilla.org/setup/configuring_build_options.html). By default, Firefox build will read the file named simply `mozconfig` for its configuration, unless the `MOZCONFIG` environmental variable is given instead.

In tor-browser, we have some existing `mozconfig` files in the top-level directory. Although you will likely only use one of these for a "dev" build: either `mozconfig-linux-x86_64-dev` or `mozconfig-macos-dev`, appropriate to your platform. To use one of these by default, you could create a symbolic link from `mozconfig` to the corresponding `-dev` file. On Linux:

```sh
ln -s mozconfig-linux-x86_64-dev mozconfig
```

Alternatively, you can create a new `mozconfig` file that sources the other. On Linux:

```sh
echo '. $topsrcdir/mozconfig-linux-x86_64-dev' > mozconfig
```

This second option will allow you to add additional lines in `mozconfig` to make adjustments, should you need them.

# Make

In the `tools/torbrowser` directory we have a `MakeFile` which contains some common commands for creating a "dev" build.

### First Build

For your first build, make sure you are in the `tools/torbrowser` directory. Then use:

```sh
# Configure
make config
# Compile
make build
# Fetch a distribution copy of Tor Browser.
make fetch
# Graft the compiled changes into the fetched distribution.
make deploy
```

Then to run the program, use:

```sh
make run
```

You can also use the `ARGS` variable to pass additional arguments, e.g.:

```sh
make run ARGS=-jsconsole
```

Note that the actual Tor Browser files can be found in the `.binaries/dev` directory, so you can go there to run and hack Tor Browser more directly.

## Future Builds

Then, after further changes, to build again you can simply run

```sh
make
```

which will run the `build` and `deploy` commands.

You won't need to run the `fetch` command as often. It is useful for pulling in newer changes to Tor Browser that are outside of tor-browser, like newer versions of the `tor` program. It is also useful as a general clean up because it will erase your browser profile and allow you to start from scratch.

## Clobber Builds

Every now and then, the Firefox build system will ask you to clobber when you run `make build`. This usually happens after checking out another ESR branch, and is a hint that you should rebuild from scratch. You can use

```sh
# Erase old build.
make clobber
# Rebuild
make
```

Note that checking out another ESR branch momentarily before switching back can also trigger a rebuild. This is because the build system looks at the modification time of specific files, rather than the repository contents, which a checkout may change. Therefore, be careful checking out older branches if you want to avoid this rebuild trigger!

