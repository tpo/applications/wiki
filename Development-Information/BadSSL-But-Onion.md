Here are a list of links that you can try out for triggering security warnings on Tor Browser. For clearnet warnings, visit [badssl.com](https://badssl.com/):

- Sanity check (HTTP-only): [sanityunhavm6aolhyye4h6kbdlxjmc7zw2y7nadbni6vd43agm7xvid.onion](http://sanityunhavm6aolhyye4h6kbdlxjmc7zw2y7nadbni6vd43agm7xvid.onion)
  - Hosted with all the other services. If down, all the other services will be also down.
- "Valid" self-signed cert: [selfiez3m4cmeo5sbqw7m7ojnln5rm3kne3ed7nyjd3ehzp4k6cdgzid.onion](https://selfiez3m4cmeo5sbqw7m7ojnln5rm3kne3ed7nyjd3ehzp4k6cdgzid.onion)
- Expired certificate: [expirezf7z2sqlnqtlnmxhy5dhiykby46v5vpp6tkm3odix3ojikpxid.onion](https://expirezf7z2sqlnqtlnmxhy5dhiykby46v5vpp6tkm3odix3ojikpxid.onion)
- Certificate that will be valid only in the future: [future2eb4tqrw6t6ka5ekdrcw5mjaxqu4rayndk775cdi6aihod7yyd.onion](https://future2eb4tqrw6t6ka5ekdrcw5mjaxqu4rayndk775cdi6aihod7yyd.onion)
- Wrong domain: [domain736gp5nirxh4iypjf2m42cy5sipj5vnecvexag6svvsi52ovyd.onion](https://domain736gp5nirxh4iypjf2m42cy5sipj5vnecvexag6svvsi52ovyd.onion)
- Onion auth (both HTTP and HTTPS): [pierovlcy7baatz7xcaccbf2kanct3hvkhcgedz4cbrmcg2ybmjalxad.onion](http://pierovlcy7baatz7xcaccbf2kanct3hvkhcgedz4cbrmcg2ybmjalxad.onion)
  - Private key: `VQCRB3BB56UZV7ZOUAHVAIV3UUPWHII3WM5RCZOSMDJKDREPDQGQ`


# How to generate a certificate

First, generate your onion address.
You can let Tor generate it (you will find it in the `hostname` file of your Onion Service directory), or you can generate a vanity one with [mkp224o](https://github.com/cathugger/mkp224o).

If you generate it, remember to change the permissions to something `tor` likes, e.g. `700` (tor will adjust the sticky bit as needed).

Once you have your domain, copy, paste, and adapt this configuration file as `config.cnf`:

```ini
[req]
default_md = sha256
prompt = no
distinguished_name = req_distinguished_name
req_extensions = v3_req
x509_extensions = san

[req_distinguished_name]
countryName = IT
organizationName = Onion Tests
commonName = yourdomainname.onion

[v3_req]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment

[san]
subjectAltName = DNS:yourdomainname.onion
```

Multiple domains can be separated by comma, but `DNS:` must be repeated for all of them.
Also, it is possible to include a wildcard domain, for example:

```ini
[san]
subjectAltName = DNS:yourdomainname.onion, DNS:*.yourdomainname.onion
```

Then run these commands:

```bash
openssl genrsa -out domain.key 4096
openssl req -config config.cnf -new -x509 -days 3650 -key domain.key -out domain.pem
```

If you want, verify the certificate you've just generated with

```bash
openssl x509 -in domain.pem -noout -text
```

In case, make sure the output contains this key, or Tor Browser will refuse to connect:

```
        X509v3 extensions:
            X509v3 Subject Alternative Name: 
                DNS:yourdomainname.onion
```

Then upload everything somewhere and configure your webserver as needed.
