# [Review Priorities for 2022](https://gitlab.torproject.org/tpo/applications/team#priorities)

-     Improve Browser from feedback from users
-     "Tor VPN" implementation
-     Improve QA process for Tor Browser
-     Sponsor work related to circumvent censorship
-     Tor Browser refactoring
-     More people in the team! 

# Sponsor for 2023

## Sponsor 9 (funded until June 2023)

    (info from thsi project can be find in the links in the top in the project meeting pad https://pad.riseup.net/p/sponsor9-keep)


    Allocation:

    <as needed>

    Commitments:

    Work on improvements coming out of usablity research

    Activity 3.2: Standardize our development cycles and integrate user feedback collection directly into the process (pending funding for after July 2023)


## Sponsor 96 - ends on July 1st but we will have a "no cost extension" for 6 more months

    (info from this project can be find in the links at the top in the project meeting pad http://kfahv6wfkbezjyg4r6mlhpmieydbebr5vkok5r34ya464gqz6c44bnyd.onion/p/s96_access_to_uncensored_internet-keep)


    Allocation:

    dan? pier?

    Commitments

    O3.1: Improve automatic censorship detection during bootstrapping in Tor Browser (desktop and Android). <- Let's include Android for next year.

    UX have usability testing running from February–Match for desktop, so expect amends


## Sponsor 30 (until May 31st)

    (info from this project can be find in the links at the top the project meeting  pad http://kfahv6wfkbezjyg4r6mlhpmieydbebr5vkok5r34ya464gqz6c44bnyd.onion/p/sponsor30-meeting-pad)


    Allocation:

    1 dev


    Commitments:

    UX Team are returning to the target region in March/April to re-test fixes to the pain points we documented in 2022 – these can be found here:


    https://gitlab.torproject.org/tpo/applications/tor-browser/-/boards?label_name[]=Sponsor%2030


    We'll need to get these in alpha by then please.


## Sponsor 131 (continue the whole year)

    Allocation:

    everybody


    Commitment:


    https://pad.riseup.net/p/s131-roadmap



# Brainstorm: Blue Sky Planning for 2023


If everything were to go perfectly, where do we aspire to be this time next year? Please add your own areas you'd like on see progress on (eg: something which would take up an entire bullet-point on next year's State of the Onion) and/or add votes to entries if you think they should be placed at a different level. Dream big and we'll see what we can do over the next year :)

Must

- ship Privacy Browser (March)

    - base browser + theming/branding, 

    - extensions

    - installers/packages

- tor-browser-bundle-testsuites modernization:
    evaluating the test landscape, determining the tests that make sense and spending time on working on improving in this area.

    - Privacy Browser needs tests too

    - get ./mach tests working

- rlbox sandbox (the MR is ready for review already :))
- switch all the Android projects to the right SDK version, and avoid another 11.5.9

    - or, even better, avoid another crash storm like 11.5.8!

- fix the crashes with assertions on

    - last time we tried, Tor Browser was not runnable in debug mode because of some race with preferences


- start prototyping arti in android on Q2

- sustainable release process
- automated testing system in place (esp for non-linux platforms)

- ESR migration starting on time.

High

- fluent+weblate localization

    - Already moved to Weblate, with old formats!

    - Fluent still blocked on Weblate/its toolkit support

- torbutton integration/refactor

    - Integrated as a copy past, so only refactor missing

    - doing this before Arti could help to better understand how to implement things for Arti/refactor to be more generic and plug Arti in an easier way then

- migrate to 'new' esr-115 on-boarding
- fix the accessibility issues
- about:torconnect on Android

    - rip out about:tor native Android UI, ideally same code as on Desktop

    - I wonder if it was originally used, and then changed for some reason (i.e., we might need more context for this)

    - or do we skip this and go straight to TorVPN integration directly (whatever this means)

    - quick connect and a way to force users through bootstrap (we have lots of issues for this)

    - replace tor-android-service with arti backend? theoretically the only feature missing is onion service client auth (granted the rest is untested/unknown)

    - Nope, onion services are still under development

    - I think that in general we need more UX time for Android (or, possibly, another UX person)

- update the Tor Browser design doc and threat model

    - kind of already knew this, but some of the disk leak issues are out of our control so we shouldn't really be promising/implying as much protection there as we do

- arti integration
- bridge URIs
- rebase to 115 asap

    - ideally, start when it goes nightly (beginning of April), and have a1 when it goes beta (beginning of May). That'd give us 4 alphas.

- solve the Go dependencies problem

    - building PTs takes 30 minutes for each platform (i.e., 5 hours!)

    - boklm already thought to a solution: group more projects together (i.e., build obfs4-only dependencies with obfs4client, Pion when building Snowflake, etc)

- enable/explore compile time flags that enhance security

Low

- sign windows bins (exes, dlls, etc)

    - Do you mean sign the installer content (+ DLLs), instead of signing only the installer? yep -richard

- be able to run multiple instances of Tor Browser

    - Also somehow related to torbutton refactor, i.e., phase 3 of the refactor:

    - phase 1: tor-launcher refactor (done)

    - phase 2: torbutton refactor

    - phase 3: integrate tor-launcher and torbutton more (if we don't do that already in phase 2), apply the golden Tor-enabled app dev rules, etc.

- regular bridge string updates from rdsys

    - as part of update ping process

- per cpu+OS update/download metrics

    - currently only per OS

    - I think this is something for the metrics team: it should be already included in the %BUILD_TARGET% (including 32-bit app in 64-bit OS)

- improved NoScript+Browser UX integration
- automatic verification of build reproducibility in the internal updater (something similar to apt-transport-in-toto, but in Tor Browser updater: https://github.com/in-toto/apt-transport-in-toto)
- solve also all the other dependencies problems

    - Java: a major Firefox update takes at least a couple of days (if you're fast enough)

    - Lots of time, but I write this here in the low priority section because now that we're on our ESR channel it's 2 days per year, which is doable, but isn't good if we want to go back to RR

    - Rust: play with Cargo.lock and cargo fetch to avoid publishing dependencies on people.tpo

    - other dependencies: should we create an update policy?

- improve performances of Tor Browser

    - LTO, PGO

- start running asan builds again

    - Our patches are mostly JS, so we should be covered by Mozilla testing, but could be still a good idea to


~Radical Ideas~

- more Tor Browser package/install formats?

    - AppImage for Linux? Portable zip archive vs System installer for Windows? 

    - Portable zip is less good for checking signatures.

    - Bundled locales gives us flexibility to explore here

    - Maybe snap and/or Flatpak? Investigate the possiiblities and make a determination

    - Should we improve the compatibility with non-portable mode also for Tor Browser?

- Tor Browser in more places?

    - homebrew for macOS, tor's apt repo, *something* on Windows?

- more supported platforms?

    - aarch64 linux+windows

    - Windows depends on work that is still being done for mingw, only works with Clang on top of MSVC (see https://firefox-source-docs.mozilla.org/build/buildsystem/supported-configurations.html)

    - ppc linux (not sure if this is worth maintaining)

- open up about:rulesets to the users?
- opt-in arti in privacy-browser for updater? (probably 2024 more likely)
- continued tor-browser-build generalization effort?
- unified font rendering implementation across platforms (goodbye ClearType?)
- benchmark+improve tor-browser-build peformance

    - we should see what is slow and determine if we can speed it up

    - add profiling instrumentation to see build time, how well each project parallelizes, etc

    - then build projects in parallel (e.g., keep a queue for projects that can use a lot of cores, such as Firefox, and one for projects that don't, e.g., the manual, go dependencies, source creation, etc)

    - try to reduce the reduncancy of host tools

    - build them once in the Jessie container and use them for all the platforms, instead of building them for Linux, then again for Windows, for macOS, etc (and if we're unlucky we have to build them once for 32-bit and once for 64-bit)

    - i.e., build Clang, Cmake, go 1.4 (the bootstrapper), etc

    - host tools are usually built only once, and then rarely updated, but it could help saving space

- write our own application services for Android that are just no-ops
- CI, instead of nightly builds
